

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class SaveDataInExcel {
	
	
	
    HSSFWorkbook workBook=new HSSFWorkbook();
    HSSFSheet sheet=workBook.createSheet("TaxDeedSales");
	

	//public static Object aa[][]=new Object[][]{} ;
	static Object [][] tempData=new Object[99][15];
	
	void addJobId(String jobId, String stepId){
		String cellRow=stepId.substring(5);
		Integer rowNumber=Integer.valueOf(cellRow)+1;
		tempData[rowNumber][0]=jobId;
		
		
	}
	
	void addStepId(String stepId){
		String cellRow=stepId.substring(5);
		Integer rowNumber=Integer.valueOf(cellRow)+1;
		tempData[rowNumber][1]="STEPID#"+rowNumber;
	}
	
	void addHeadersToExcel(){
		tempData[0][0]="JOB ID";
		tempData[0][1]="STEP#id";
		tempData[0][2]="PARCEL NUMBER";
		tempData[0][3]="OVERBID AMOUNT";
		tempData[0][4]="ASSESSED PARTY";
		tempData[0][5]="TITLE SEARCH REPORT LINK";
		tempData[0][6]="INTERESTED PARTIES NAME";
		tempData[0][7]="INTERESTED PARTIES NAME2";
		tempData[0][8]="INTERESTED PARTIES ADDRESS1";
		tempData[0][9]="INTERESTED PARTIES ADDRESS2";
		tempData[0][10]="INTERESTED PARTIES CITY";
		tempData[0][11]="INTERESTED PARTIES STATE";
		tempData[0][12]="INTERESTED PARTIES ZIP";
		
	}
	
	
	void tempObject(int row, int column, String value){
		tempData[row][column]=value;
		}
	
	void findCell(String stepId,String ExtractedValue,String columnName){
		String cellRow=stepId.substring(5);
		Integer rowNumber=Integer.valueOf(cellRow)+1;
		switch(columnName){
		case "parcelNumber":{tempObject(rowNumber,2,ExtractedValue);break;}
		case "overbidValue":{tempObject(rowNumber,3,ExtractedValue);break;}
		case "assessedParty":{tempObject(rowNumber,4,ExtractedValue);break;}
		case "titleSearchReport":{tempObject(rowNumber,5,ExtractedValue);break;}
		case "interestedPartiesDetails1":{tempObject(rowNumber,6,ExtractedValue);break;}
		case "interestedPartiesDetails2":{tempObject(rowNumber,7,ExtractedValue);break;}
		case "interestedPartiesDetails3":{tempObject(rowNumber,8,ExtractedValue);break;}
		case "interestedPartiesDetails4":{tempObject(rowNumber,9,ExtractedValue);break;}
		case "interestedPartiesDetails5":{tempObject(rowNumber,10,ExtractedValue);break;}
		case "interestedPartiesDetails6":{tempObject(rowNumber,11,ExtractedValue);break;}
		case "interestedPartiesDetails7":{tempObject(rowNumber,12,ExtractedValue);break;}
		case "interestedPartiesDetails8":{tempObject(rowNumber,13,ExtractedValue);break;}
		
		
	  }
	 }
	
	void saveDataInExcel() throws IOException{
		int rowCount = 0;
	    for (Object[] tempValue : tempData) {
	     Row row = sheet.createRow(rowCount++);
	     
	     int columnCount = 0;
	     
	     for (Object field : tempValue) {
	         Cell cell = row.createCell(columnCount);
	         if (field instanceof String) {
	             cell.setCellValue((String) field);
	             columnCount=columnCount+1;
	         } else if (field instanceof Integer) {
	             cell.setCellValue((Integer) field);
	             columnCount=columnCount+1;
	         }
	     }
	     
	 }
	    FileOutputStream d1=new FileOutputStream (new File("OrangeCountyTaxDeedSales.xls"));
        workBook.write(d1);	
	}
	
	
	public static void main(String[] args) {
		
		SaveDataInExcel t1=new SaveDataInExcel();
		try {
			t1.findCell("Step#5", "ravisharma", "overbidValue");
			t1.findCell("Step#5", "r@a7889", "parcelNumber");
			t1.findCell("Step#5", "Party", "AssessedParty");
			t1.findCell("Step#5", "title", "TitleSearchRepor");
			t1.findCell("Step#5", " ", "InterestedPartiesName");
			t1.findCell("Step#5", "partyname2", "InterestedPartiesName2");
			
			
			t1.saveDataInExcel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}

}
