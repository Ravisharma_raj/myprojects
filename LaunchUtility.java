
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LaunchUtility {
	protected static HtmlUnitDriver driver;
	protected static WebDriverWait webDriverWait;
	public static void main(String[] args) {
		
		try{
		
			ScreenNavigation.screenNavigation();

		}catch(Exception e){
			
			System.out.println(e.getMessage());
			
		}finally{
			
			driver.quit();
			}
		
	}
		
	
}


