
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class ScreenNavigation extends LaunchUtility{
	
	 private static WebDriverWait webDriverWait;
	 private static String saleDate;
	 private static boolean saleDateExist=false;
	 protected static String stepId;
	 static SaveDataInExcel objSaveInExcel=new SaveDataInExcel();
	 static String jobId;
	 
	 
	 public static String getDate()
     {
            Date date=new Date();
            String [] Date;
            Date=date.toString().split(" ");
            String currentDate=Date[1]+" "+Date[2]+", "+Date[5];
            System.out.println(currentDate);
            return currentDate;
           
     }
	 
	 public static void screenNavigation(){
		    saleDate=getDate();
		    saleDate="May 5, 2016";
		    //File file = new File("C:\\Program Files\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");             
	        //System.setProperty("phantomjs.binary.path", file.getAbsolutePath());   
		    driver=new HtmlUnitDriver();
		    driver.navigate().to("http://or.occompt.com/recorder/web/login.jsp");
			driver.findElement(By.cssSelector("input[value=\"I Acknowledge\"]")).click();
			driver.findElement(By.partialLinkText("Tax Deed Sales")).click();
			Select saleDateList=new Select(driver.findElement(By.id("SaleDateID")));
		  	List<WebElement>  saleDateOptions=saleDateList.getOptions();
		  	for(WebElement webElement:saleDateOptions){
		  		if(saleDate.equalsIgnoreCase(webElement.getText().trim())){
		  			saleDateExist=true;
		  			break;
		  			} //end if                                                                
		  	}
		  	
		  	if(saleDateExist==true){
		  		
		  		driver.findElement(By.xpath("//option[contains(text(), 'Active Overbid')]")).click();
				//driver.findElement(By.xpath("//option[contains(text(), 'Apr 25')]")).click();
				   
					driver.findElement(By.xpath("//option[contains(text(),'"+saleDate+"')]")).click();
					driver.findElement(By.xpath("//input[@value='Search']")).click();
				    new Select(driver.findElement(By.id("sort_page_size"))).selectByVisibleText("100");
					String url=driver.getCurrentUrl();
					//List<WebElement> taxSalesRecord=driver.findElements(By.xpath("//*[@id='searchResultsTable']/tbody/tr"));
					List<WebElement> taxSalesRecord=driver.findElements(By.xpath("//a[contains(text(), 'Tax Sale')]"));
					if(taxSalesRecord.size()>0){
						try {
							SaveDataInDatabase.connectToDatabase();
							jobId=SaveDataInDatabase.generateJobId();
							SaveDataInDatabase.updateJobIdToDataTable(jobId);
							
							
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						int salesRecordCount=taxSalesRecord.size();
						webDriverWait=new WebDriverWait(driver,30);
					
						objSaveInExcel.addHeadersToExcel();
						for(int i=0;i<salesRecordCount;i++)
						{
							driver.navigate().to(url);
							//new Select(driver.findElement(By.id("sort_page_size"))).selectByVisibleText("100");
							//taxSalesRecord=driver.findElements(By.xpath("//*[@id='searchResultsTable']/tbody/tr"));
							taxSalesRecord=driver.findElements(By.xpath("//a[contains(text(), 'Tax Sale')]"));
							taxSalesRecord.get(i).click();
							String parcelNumber=ExtractingData.getParcelNumber(driver).trim();
							
							try {
								if(SaveDataInDatabase.checkParcelNumberinDataTable(parcelNumber)==false) {
									stepId="Step#"+i;
									objSaveInExcel.addJobId(jobId,stepId);
									objSaveInExcel.addStepId(stepId);
									ExtractingData.addStepId();
									driver.findElement(By.partialLinkText("Overbid")).click();
									ExtractingData.extractOverbidAmount(driver);
									ExtractingData.extractParcelNumber(driver);
									driver.findElement(By.partialLinkText("Parties")).click();
									ExtractingData.interestedPartiesDetails(driver);
									ExtractingData.fetchTitleSearchReportLink(driver);
									ExtractingData.extractAssessedParty(driver);
									}
									else {System.out.println("Duplicate Parcel Number"+parcelNumber);}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
					 
				        }//end for loop
						try {
							objSaveInExcel.saveDataInExcel();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							System.out.println(e.getMessage());
						}
						
					}else{
						
						System.out.println("No Result found for Active Overbid and sales date");
					    //driver.quit();
						
						}//end else
				}else{
					
					System.out.println("Current date "+saleDate+" is not a sale date");
					//driver.quit();
				     }//end else
		  		
		  	//driver.quit();
			}//end screennavigation
	 
		}
