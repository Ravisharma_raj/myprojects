
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ExtractingData extends ScreenNavigation{
	
	public static void addStepId() throws SQLException{
		SaveDataInDatabase.addstepIdDataTable(stepId);
		
		
	}
	
	public static String getParcelNumber(WebDriver driver){
		WebElement parcelNumber=driver.findElement(By.xpath("//span[contains(text(),'Parcel Number')]/following-sibling::span[1]/span"));
		String parcelNumberValue=parcelNumber.getAttribute("innerHTML");
		parcelNumberValue=parcelNumberValue.replace("&nbsp;", "");
		parcelNumberValue=parcelNumberValue.replace("<span class=\"text\">", "");
		parcelNumberValue=parcelNumberValue.replace("</span>", "").trim();
		return parcelNumberValue;
	}

	public static int extractOverbidAmount(WebDriver driver) throws SQLException{
		WebElement overbidAmount=driver.findElement(By.xpath("//span[contains(text(),'Overbid Amount')]/following-sibling::span[1]/span"));
		String overbidAmountValue=overbidAmount.getAttribute("innerHTML");
	      
		overbidAmountValue=overbidAmountValue.replace("&nbsp;", "");
		overbidAmountValue=overbidAmountValue.replace("<span class=\"text\">", "");
		overbidAmountValue=overbidAmountValue.replace("</span>", "");
		SaveDataInDatabase.addOverbidAmountToDataTable(overbidAmountValue, stepId);
		objSaveInExcel.findCell(stepId, overbidAmountValue, "overbidValue");
		System.out.println(overbidAmountValue);
		return 1;
		
	}
	
	public static String extractParcelNumber(WebDriver driver) throws SQLException{
		WebElement parcelNumber=driver.findElement(By.xpath("//span[contains(text(),'Parcel Number')]/following-sibling::span[1]/span"));
		String parcelNumberValue=parcelNumber.getAttribute("innerHTML");
		parcelNumberValue=parcelNumberValue.replace("&nbsp;", "");
		parcelNumberValue=parcelNumberValue.replace("<span class=\"text\">", "");
		parcelNumberValue=parcelNumberValue.replace("</span>", "").trim();
		objSaveInExcel.findCell(stepId,parcelNumberValue, "parcelNumber");
		if (SaveDataInDatabase.checkParcelNumberinDataTable(parcelNumberValue)==false){
		SaveDataInDatabase.addParcelNumberToDataTable(parcelNumberValue, stepId);
		System.out.println(parcelNumberValue);
		objSaveInExcel.findCell(stepId,parcelNumberValue, "parcelNumber");
		
		return parcelNumberValue ;
		}else{return "Parcel Number allready exist";}
	}
	
	
	
	public static int extractAssessedParty(WebDriver driver) throws SQLException{
		WebElement assessedParty=driver.findElement(By.xpath("//legend[contains(text(),'Assessed Party')]/following-sibling::span[2]/span"));
		String assessedPartyValue=assessedParty.getAttribute("innerHTML");
		assessedPartyValue=assessedPartyValue.replace("&nbsp;", "");
		assessedPartyValue=assessedPartyValue.replace("<span class=\"text\">", "");
		assessedPartyValue=assessedPartyValue.replace("</span>", "");
		System.out.println("this is asseseed party"+assessedPartyValue);
		SaveDataInDatabase.addAssessedPartyToDataTable(assessedPartyValue, stepId);
		objSaveInExcel.findCell(stepId,assessedPartyValue,"assessedParty");
		
		return 1;
		
	}
	
	public static HashMap<String, String> interestedPartiesDetails(WebDriver driver) throws SQLException{
		int columnCount=1;
		HashMap<String,String> interestedPartiesDetailsValues=new HashMap<String,String>();
		List<WebElement> interestedPartiesDetails=driver.findElements(By.xpath("//*[contains(text(),'Interested Parties')]/following-sibling::table[1]/tbody/tr[2]/td/span"));
		for(WebElement columnValue: interestedPartiesDetails){
			String columnValueText=columnValue.getAttribute("innerHTML");
			columnValueText=columnValueText.replace("&nbsp;", "");
			columnValueText=columnValueText.replace("<span class=\"text\">", "");
			columnValueText=columnValueText.replace("</span>", "");
			interestedPartiesDetailsValues.put("Column"+columnCount, columnValueText);
			SaveDataInDatabase.addInterestedPartiesFirstRowToDataTable("Column"+columnCount, columnValueText,stepId);
			objSaveInExcel.findCell(stepId,columnValueText,"interestedPartiesDetails"+columnCount);
			System.out.println(columnValueText);
			columnCount=columnCount+1;
		}
		return interestedPartiesDetailsValues;
		}
	
	public static boolean fetchTitleSearchReportLink(WebDriver driver) throws SQLException{
		
		WebElement titleSearchLink=driver.findElement(By.partialLinkText("View Title Search Report"));
		String txtLink=titleSearchLink.getAttribute("href");
		System.out.println(titleSearchLink.getAttribute("href"));
		SaveDataInDatabase.addTitleSearchReportLinkToDataTable(txtLink, stepId);
		objSaveInExcel.findCell(stepId,txtLink,"titleSearchReport");
		
		return true;
		
		
		
	}
}
