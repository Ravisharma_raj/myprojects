

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public class SaveDataInDatabase extends DataBaseConnection {
	public static String jobId;
	
	@SuppressWarnings("deprecation")
	public static String generateJobId(){
		 Date date=new Date();
	     jobId="TaxDeedSalesDataExtraction#"+date.getMonth()+date.getDay()+date.getMinutes()+date.getSeconds();
	     System.out.println("The job id is TaxDeedSalesDataExtraction#"+date.getMonth()+date.getDay()+date.getMinutes()+date.getSeconds());
		 return jobId;
	}
	
	public static boolean checkParcelNumberinDataTable(String parcelNumber) throws SQLException{
		ResultSet rs=st.executeQuery("Select * from DataExtraction.TaxDeedSales where ParcelNumber ='"+parcelNumber+"';");
		
		return rs.next();
	}
	
	public static void connectToDatabase() throws SQLException{
		DataBaseConnection.connectToDataBase();
		st=conn.createStatement();
		//SaveDataInDatabase.generateJobId();
		//SaveDataInDatabase.updateJobIdToDataTable(jobId);
		}
	  public static boolean updateJobIdToDataTable(String jobId) throws SQLException{
		 st.executeUpdate("insert into DataExtraction.TaxDeedSales (JobId) values ('"+jobId+"')");
		 return true;
		 }	
	  
	  public static boolean addstepIdDataTable(String stepId) throws SQLException{
			//st.execute("Update TaxDeedSales set StepId='"+stepId+"' where JobId="+jobId+")");
			st.executeUpdate("insert into DataExtraction.TaxDeedSales (JobId,StepId) values ('"+jobId+"','"+stepId+"');");
			return true;
			}
	
		public static boolean addParcelNumberToDataTable(String parcelNumber,String stepId) throws SQLException{
			st.executeUpdate("Update DataExtraction.TaxDeedSales set ParcelNumber='"+parcelNumber+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			return true;
			}
		
		public static boolean addOverbidAmountToDataTable(String overbidAmount, String stepId) throws SQLException{
			st.executeUpdate("Update DataExtraction.TaxDeedSales set OverbidAmount='"+overbidAmount+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			return true;
			}
		
		public static boolean addAssessedPartyToDataTable(String assessedParty,String stepId) throws SQLException{
			st.executeUpdate("Update DataExtraction.TaxDeedSales set AssessedParty='"+assessedParty+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			return true;
			}
		
		public static boolean addInterestedPartiesFirstRowToDataTable(String interestedPartiesColumnName, String columnValue, String stepId) throws SQLException{
			
			switch (interestedPartiesColumnName){
			case "Column1":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesName='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column2":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesName2='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column3":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesAddress1='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column4":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesAddress2='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column5":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesCity='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column6":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesState='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column7":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesZip='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
			case "Column8":st.executeUpdate("Update DataExtraction.TaxDeedSales set InterestedPartiesCountry='"+columnValue+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			break;
				}
			
			return true;
			}
		
		public static boolean addTitleSearchReportLinkToDataTable(String reportLink,String stepId) throws SQLException{
			st.execute("Update DataExtraction.TaxDeedSales set TitleSearchReport='"+reportLink+"' where JobId='"+jobId+"' and StepId='"+stepId+"';");
			return true;
			}
		
	}	
	


